# DisasterMessageBoard

ウェブ通知や位置情報のマップ表示を既存のサービスに追加した災害用伝言版

## Usage
1. Web Browserで`https://anpi.ruther.tk`にアクセス
2. 指示通りにAccount作成

## Library & Programming Language
- [PHP 7.2](https://www.php.net)
- [materializecss](https://materializecss.com)
- [MySQL 5.7](https://www.mysql.com/)
- apache 2.4
- [Yahoo! Open Local Platform](https://developer.yahoo.co.jp/webapi/map/)