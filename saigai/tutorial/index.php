<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>使い方のページ</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<style>
		.bold {
			font-weight: bolder;
		}
		strong {
			font-weight: bold;
			background: linear-gradient(rgba(0,0,0,0) 70%,#ffff66 50%);
		}
		li {
			padding-top: 20px;
			margin: auto;
		}
	</style>
</head>
<body class="container">
	<h1>使い方のページ</h1>
	<div class="row left">
		<div class="col s12">
			<h3>このサイトについて</h3>
			<div class="blue-text text-darken-3">
				このサイトは、ドットゼミEXPO2018 中学生部門で第三位になった作品です。
			</div>
			<p>
				この作品は、災害用伝言板サービスを提供することができます。
			</p>
			<!--<div class="col s5 m12"></div>-->
			<table class="col s12 m12">
				<tr>
					<th>
						<a href="https://anpi.ruther.tk/privacy-policy">
							プライバシーポリシー
						</a>
					</th>
					<th>
						<a href="https://anpi.ruther.tk/terms">
							利用規約
						</a>
					</th>
				</tr>
			</table>
			<!--<div class="col s5 m12"></div>-->
			<h3 class="col s12">利用方法</h3>
			<h4 class="">1.アカウント登録</h4>
			<ol class="col s12 m12">
				<li><a href="https://system.ruther.tk/signup/">新規登録ページ</a>に移動して画面の指示通りに入力して、<strong>新規登録</strong>を押して新規登録してください</li>
				<li><strong>戻る</strong>を押して<strong>ログイン</strong>を押して先程入力した情報を入力してログインしてください</li>
			</ol>
			<h4>2.安否確認</h4>
			<ol class="col s12 m12">
			    <li><strong>安否情報を確認する</strong>を押し、災害用伝言板サービスのページに移動してください</li>
			</ol>
			<h5>2.2 送信</h5>
			<ol class="col s12 m12">
			    <li><strong>安否情報を送信する</strong>を押して送信ページへ移動してください</li>
				<li><strong>ドロップダウンメニュー</strong>から該当する状況を選択し、<strong>位置情報を自動取得する</strong>を押すか<strong>経度と緯度</strong>の両方の値を入れ<strong>経度と緯度から住所を特定する</strong>を押して住所を補完し、<strong>送信する</strong>を押して送信してください</li>
				<div style="color: #999999">位置情報を自動取得した場合は「経度と緯度から住所を特定する」を押さなくても大丈夫です</div>
				<li><strong>送信完了</strong>という文字が出ていれば送信が完了しました</li>
			</ol>
		</div>
	</div>
</body>
</html>