<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	$_SESSION["url"] = 'https://anpi.ruther.tk/';
}else{
	$_SESSION["url"] = '';
}
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130431814-1');
	</script>
	<meta charset="utf-8">
	<title>災害用伝言版</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	 <!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js" async></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body class="container">
	<h1>災害用伝言板</h1>
	<!-- ユーザーIDにHTMLタグが含まれても良いようにエスケープする -->
	<?php if (isset($_SESSION['NAME'])): ?>
		<p class="gray-text center">ようこそ<u><?php echo htmlspecialchars($_SESSION["NAME"], ENT_QUOTES); ?></u>さん</p>  <!-- ユーザー名をechoで表示 -->
		<p class="center"><a href="https://system.ruther.tk/logout">ログアウト</a></p>
	<?php else: ?>
		<p class="center"><a href="https://system.ruther.tk/signup">新規作成</a></p>
		<p class="center"><a href="https://system.ruther.tk/login">ログイン</a></p>
	<?php endif; ?>
	<form action="send/" class="center">
		<input type="submit" class="btn-flat blue-text text-darken-2" value="安否情報を送信する">
	</form>
	<form action="map/" class="center">
		<input type="submit" class="btn-flat blue-text text-darken-2" value="安否情報を確認する">
	</form>
	

	<?php if(false/*!isset($_SESSION['LastUpdate'])*/): ?>
	<div class="row">
		<h4>初めての方へ</h4>
		<div class="col s2"></div>
		<div class="col s10">
			このサイトはドットゼミEXPO 2018で発表・リリースした通知付き災害用伝言板サービスです。
			<br>
			個人で運営しておりますので、完全なサービスを提供できない場合があります。詳しくは<a href="https://anpi.ruther.tk/tutorial" target="_blank">こちら</a>
		</div>
	</div>
	<?php endif;?>

	<form action="https://system.ruther.tk/main" class="center" style="bottom: 0px">
		<button type="submit" class="btn waves-effect white black-text" value="元に戻る">
			戻る
			<i class="material-icons left">arrow_back</i>
		</button>
	</form>

	<br>
	<hr>
	<form action="https://system.ruther.tk/mypage" class="center">
		<input type="submit" value="マイページ" class="btn-flat blue-text text-darken-2">
	</form>
	<form action="https://system.ruther.tk/option" class="center">
		<input type="submit" value="設定" class="btn-flat blue-text text-darken-2">
	</form>
</body>
</html>