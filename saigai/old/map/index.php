<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	$_SESSION["url"] = 'https://anpi.ruther.tk/map/';
}else{
	$_SESSION["url"] = '';
}
?>
<?php 
	if(isset($_SESSION['ID'])) {
		$err = '';
		$id = $_SESSION['ID'];
		$db['host'] = "localhost";		// DBサーバのURL
		$db['user'] = "nomiphp_ruther";		// ユーザー名
		$db['pass'] = "nomi051914";		// ユーザー名のパスワード
		$db['dbname'] = "nomiphp_login";		// データベース名

		try {
			$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
			$stmt = $pdo->prepare("SELECT * FROM userData WHERE id = ?");
			$stmt->execute(array($id));
			if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$lat = $row['latitude'];
				$lng = $row['longitude'];
				$ua = $row['ua'];
				$safety = $row['safety'];
				$add = $row['location'];
				$last = $row['LastUpdate'];

				function isSafety($safety) {
					switch ($safety) {
						case 'genki':
							# code...
							return '元気です';
							break;
						
						case 'kega':
							# code...
							return 'けがをしています';
							break;

						case 'help':
							# code...
							return '救助が必要です';
							break;

						default:
							# code...
							return false;
							break;
					}
				}

				function get_info($ua){
					$browser_name = $browser_version = $webkit_version = $platform = NULL;
					$is_webkit = false;
					//Browser
					if(preg_match('/Edge/i', $ua)){	
						$browser_name = 'Edge';
						if(preg_match('/Edge\/([0-9.]*/', $ua, $match)){
							$browser_version = $match[1]; 
						}
					}elseif(preg_match('/(MSIE|Trident)/i', $ua)){
						$browser_name = 'IE';
						if(preg_match('/MSIE\s([0-9.]*)/', $ua, $match)){
							$browser_version = $match[1];
						}elseif(preg_match('/Trident\/7/', $ua, $match)){
							$browser_version = 11;
						}
					}elseif(preg_match('/Presto|OPR|OPiOS/i', $ua)){
						$browser_name = 'Opera';
						if(preg_match('/(Opera|OPR|OPiOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];		
					}elseif(preg_match('/Firefox/i', $ua)){
						$browser_name = 'Firefox';
						if(preg_match('/Firefox\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
					}elseif(preg_match('/Chrome|CriOS/i', $ua)){
						$browser_name = 'Chrome';
						if(preg_match('/(Chrome|CriOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];
					}elseif(preg_match('/Safari/i', $ua)){
						$browser_name = 'Safari';
						if(preg_match('/Version\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
					}
					//Webkit
					if(preg_match('/AppleWebkit/i', $ua)){
						$is_webkit = true;
						if(preg_match('/AppleWebKit\/([0-9.]*)/', $ua, $match)) $webkit_version = $match[1];
					}
					//Platform
					if(preg_match('/ipod/i', $ua)){
						$platform = 'iPod';
					}elseif(preg_match('/iphone/i', $ua)){
						$platform = 'iPhone';
					}elseif(preg_match('/ipad/i', $ua)){
						$platform = 'iPad';
					}elseif(preg_match('/android/i', $ua)){
						$platform = 'Android';
					}elseif(preg_match('/windows phone/i', $ua)){
						$platform = 'Windows Phone';
					}elseif(preg_match('/linux/i', $ua)){	
						$platform = 'Linux';
					}elseif(preg_match('/macintosh|mac os/i', $ua)) {
						$platform = 'Mac';		
					}elseif(preg_match('/windows/i', $ua)){
						$platform = 'Windows';
					}
					return array(	
						'ua' => $ua,
						'browser_name' => $browser_name,
						'browser_version' => intval($browser_version),
						'is_webkit' => $is_webkit,
						'webkit_version' => intval($webkit_version),
						'platform' => $platform
					);
				}
			};
			function isGarake($ua) {
				$return = '';
				if((strpos($ua, 'DoCoMo') !== false) || (strpos($ua, 'FOMA') !== false)) {
					$return = 'docomo';
					return true;
				}else if(strpos($ua, 'SoftBank') !== false) {
					$return = 'SoftBank';
					return true;
				}else {
					return false;
				}
			}
			$browser_info = get_info($ua);
			$is = isGarake($_SERVER['HTTP_USER_AGENT']);
			$saf = isSafety($safety);

		} catch (Exception $e) {
			$err = 'Error:' . $e;
		}
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130431814-1');
	</script>
	<meta charset="utf-8">
	<title>安否情報の確認</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Compiled and minified CSS -->
	<?php if(!$is) : ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
		<!-- Compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
		<link rel=“stylesheet” href=“https://cdn.jsdelivr.net/npm/fontisto@v3.0.4/css/fontisto/fontisto.min.css”>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="icon" href="https://img.ruther.tk/icon.png">
	<?php endif; ?>
</head>
<body>
	<h1>安否情報の確認</h1>
	<?php if (isset($_SESSION['NAME'])): ?>
		<p class="gray-text center">ようこそ<u><?php echo htmlspecialchars($_SESSION["NAME"], ENT_QUOTES); ?></u>さん</p>		<!-- ユーザー名をechoで表示 -->
		<p class="center"><a href="https://system.ruther.tk/logout">ログアウト</a></p>
		<div id="map" style="width: 100%; height: 300px; margin: auto;" class="center"></div>
		<!--<div class="center gray-text" style="height: 200px;">-->
			<?php if(/*(isset($lat) || !empty($lat)) && (isset($lng) || !empty($lng))*/ $last !== "0000-00-00 00:00:00") : ?>
				<div style="width: 100%; font-size: 4.5vw;" class="center"><b>最後に更新された位置情報を表示しています。</b></div>
				<br>
				<table>
					<!--<div style="margin: 0px; width: 50%; height: 100%; float: left; text-align: right;"><h2 style="margin-top: 0px; margin-right: 5%;">安否情報</h2></div>
					<div style="width: 50%; height: 100%; float: left;" class="center">-->
						<tr>
							<th>安否情報</th>
							<td>
								<table>
									<tr>
										<th class="blue-text text-darken-3">安否情報:</th><td><?php if (isset($saf)) { echo $saf; } ?></td>
									</tr>
									<tr>
										<th class="blue-text text-darken-3">住所:</th><td><?php if(isset($add)) { echo (String)mb_convert_encoding($add, "UTF-8"); } ?></td>
									</tr>
									<tr>
										<th class="blue-text text-darken-3">経度・緯度:</th><td><?php if(isset($lng) && isset($lat)) {echo $lng . ' , ' . $lat; } ?></td>
									</tr>
									<tr>
										<th class="blue-text text-darken-3">最終更新:</th><td><?php if (isset($last)) { echo $last; } ?></td>
									</tr>
								</table>
							</td>
						</tr>
					<!--
					</div>
					<div style="margin: 0px; width: 50%; height: 100%; float: left; text-align: right;"><h2 style="margin-top: 0px; margin-right: 5%;">端末情報</h2></div>
					<div style="width: 50%; height: 100%; float: left;" class="center">
					-->
						<tr>
							<th>端末情報</th>
							<td>
								<table>
									<tr>
										<th class="blue-text text-darken-3">ブラウザ名:</th><td><?php echo $browser_info['browser_name']; ?></td>
									</tr>
									<tr>
										<th class="blue-text text-darken-3"">端末名:</th><td><?php if($browser_info['browser_name'] != NULL) {echo $browser_info['platform'];}else{echo $ua;} ?></td>
									</tr>
								</table>
							</td>
						</tr>
					<!--</div>-->
				</table>
			<?php else: ?>
				<div class="row">
					<h4>初めての方へ</h4>
					<div class="col s2"></div>
					<div class="col s10">
						このサイトはドットゼミEXPO 2018で発表・リリースした通知付き災害用伝言板サービスです。
						<br>
						個人で運営しておりますので、完全なサービスを提供できない場合があります。詳しくは<a href="https://anpi.ruther.tk/tutorial" target="_blank">こちら</a>
					</div>
				</div>
				<div style="width: 100%; font-size: 4.5vw;" class="center"><b>最後に更新された位置情報を取得できませんでした。安否情報の登録は<a href="../send/">こちら</a>から登録できます。</b></div>
				<br>
			<?php endif ; ?>
		<!--</div>-->
		<script type="text/javascript" charset="utf-8" src="https://map.yahooapis.jp/js/V1/jsapi?appid=dj00aiZpPXVERERNdms1blEzNyZzPWNvbnN1bWVyc2VjcmV0Jng9NTU-"></script>
		<script type="text/javascript">
			window.onload = function(){
				//var control = new Y.SearchControl;
				var control2 = new Y.SliderZoomControlVertical;
				var ymap = new Y.Map("map");
				ymap.drawMap(new Y.LatLng(<?php if(isset($lat ,$lng)) {echo $lat . ',' . $lng;} ?>), 17, Y.LayerSetId.NORMAL);
				//ymap.addControl(control);
				ymap.addControl(control2);
				ymap.setConfigure('scrollWhellZoom', true);
				var markerClicked = function(){
					var ll = this.getLatLng();
					alert(ll.toString());
				}
				var marker_a = new Y.Marker(new Y.LatLng(<?php if(isset($lat ,$lng)) {echo $lat . ',' . $lng;} ?>));
				var marker_b = new Y.Label(new Y.LatLng(<?php if(isset($lat ,$lng)) {echo $lat . ',' . $lng;} ?>), "最後に更新された位置");

				//marker_a.bind('click', markerClicked);

				ymap.addFeature(marker_a);
				ymap.addFeature(marker_b);

				/////////////////////////////////////////////
				$(window).resize(function(){

					var control2 = new Y.SliderZoomControlVertical;
					var ymap = new Y.Map("map");
					ymap.drawMap(new Y.LatLng(<?php if(isset($lat ,$lng)) {echo $lat . ',' . $lng;} ?>), 17, Y.LayerSetId.NORMAL);
					//ymap.addControl(control);
					ymap.addControl(control2);
					ymap.setConfigure('scrollWhellZoom', true);
					var markerClicked = function(){
						var ll = this.getLatLng();
						alert(ll.toString());
					}
					var marker_a = new Y.Marker(new Y.LatLng(<?php if(isset($lat ,$lng)) {echo $lat . ',' . $lng;} ?>));
					var marker_b = new Y.Label(new Y.LatLng(<?php if(isset($lat ,$lng)) {echo $lat . ',' . $lng;} ?>), "最後に更新された位置");

					//marker_a.bind('click', markerClicked);

					ymap.addFeature(marker_a);
					ymap.addFeature(marker_b);

				})
			}
		</script>
	<?php else: ?>
		<p class="center"><a href="https://system.ruther.tk/signup">新規作成</a></p>
		<p class="center"><a href="https://system.ruther.tk/login">ログイン</a></p>
	<?php endif; ?>
	<form action="../" class="center" style="bottom: 0px">
		<button type="submit" class="btn waves-effect white black-text" value="元に戻る">
			戻る
			<i class="material-icons left">arrow_back</i>
		</button>
	</form>
</body>
</html>