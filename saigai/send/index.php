<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	//changed from testothersend.php
	$_SESSION["url"] = 'https://anpi.ruther.tk/send/';
}else{
	$_SESSION["url"] = '';
}
?>
<?php 
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$message = '';
	$re = '';
	$reslt = 'yaaa,';
	$db['host'] = "localhost";	// DBサーバのURL
	$db['user'] = "nomiphp_ruther";	// ユーザー名
	$db['pass'] = "nomi051914";	// ユーザー名のパスワード
	$db['dbname'] = "nomiphp_login";	// データベース名

	function isN($notifi) {
		switch ($notifi) {
			case 'on':
				return true;
				# code...
					break;
				
			case 'off':
				return false;
				# code...
				break;
		}
	};

	if(isset($_POST["submit"])){
		$reslt .= 'submit,';
		if(empty($_POST['safety']) || $_POST['safety'] === 'noselect'){
			$message = '安否情報が選択されていません';
			$reslt .= 'noAnpi,';
		}elseif (empty($_POST['longitude']) && empty($_POST['latitude']) && empty($_POST['adress'])) {
			$message = '位置情報が入力されていません';
			$reslt .= 'noLocation,';
		}elseif (empty($_POST['longitude']) && !empty($_POST['latitude']) && empty($_POST['adress'])) {
			$message = '経度が入力されていません';
			$reslt .= 'noLongitude,';
		}elseif (empty($_POST['latitude']) && !empty($_POST['longitude']) && empty($_POST['adress'])) {
			$message = '緯度が入力されていません';
			$reslt .= 'noLatitude,';
//		}elseif (!empty($_POST['longitude']) && !empty($_POST['latitude']) && empty($_POST['adress'])) {
//			$message = '住所が入力されていません。';
//			$reslt .= 'noAdress,';
		}elseif ((!empty($_POST['longitude']) && !empty($_POST['latitude'])) && !empty($_POST['adress'])) {
			$reslt .= 'ok,';
			if (!empty($_POST['longitude']) && !empty($_POST['latitude']) && !empty($_POST['safety']) && !empty($_POST['adress'])) {
				$reslt .= 'LongitudeAndLatitudeAndAdress,';
				$longitude = $_POST['longitude'];
				$latitude = $_POST['latitude'];
				$safety = $_POST['safety'];

				$adress = $_POST['adress'];
				$id = $_SESSION['ID'];
				$memo = $_POST['memo'];
				try {
					$reslt .= 'andTry,';
					$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );

					$date = new DateTime();
					$date = $date->format('Y-m-d H:i:s');

					$url = 'https://anpi.ruther.tk/latlng?lat=' . $latitude . '&lng=' . $longitude;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					//Locationをたどる
					curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
					//最大何回リダイレクトをたどるか
					curl_setopt($ch,CURLOPT_MAXREDIRS,10);
					//リダイレクトの際にヘッダのRefererを自動的に追加させる
					curl_setopt($ch,CURLOPT_AUTOREFERER,true);
					//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: text/json, application/xml'));
					$res = curl_exec($ch);
					curl_close($ch);
					$argres = json_decode($res, TRUE);
					//軽度 緯度を住所に変換する
					$prefecture = $argres['prefecture'];
					$municipality = $argres['municipality'];
					$section = $argres['section'];
					$banchi = $argres['banchi'];
					$backlng = $argres['backlng'];
					$backlat = $argres['backlat'];
					$LocalLng = $argres['LocalLng'];
					$LocalLat = $argres['LocalLat'];

					$ad = $prefecture . $municipality . $section . $banchi . '番地';
					$stmt = $pdo->prepare("INSERT anpiData SET safety = ? , longitude = ? , latitude = ? , updateDate = ? , location = ? , ua = ?, memo = ?, ownerID = ?");
					$stmt->execute(array($safety, $longitude, $latitude, $date, $ad, $ua, $memo, $id));
					$re = '送信完了';
					//トークンの取得
					$stmt = $pdo->prepare('SELECT * FROM userData WHERE id = ?');
					$stmt->execute(array($id));
					//FCMに送信
					if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
						if (isN($row['isNotifi'])) {
							try{
								$st = $pdo->prepare('SELECT * FROM pushToken WHERE ownerID = ?');
								$st->execute(array($id));

								$unnoti = $st->fetchall(PDO::FETCH_ASSOC);
								foreach ($unnoti as $value) {
									$url2 = 'https://fcm.googleapis.com/fcm/send';
									$ch2 = curl_init();
									$data = array(
										'notification' => array(
											'title' => '災害用伝言板',
											'body' => 'あなたのアカウントに安否情報が送信されました。確認してください: ' . $row['LastUpdate'],
											'icon' => 'https://img.ruther.tk/icon.png',
											'click_action' => 'https://anpi.ruther.tk/map/'
										),
										'to' => $value
									);
									$data_json = json_encode($data);
									curl_setopt($ch2, CURLOPT_URL, $url2);
									curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
										'content-type: application/json',
										'authorization: key=AAAATORrppw:APA91bHdcfJtDbTVsy-JSdgiSkQH-TF6cyBmXaX94FAdTxvL0U1QCDVgFyzCy0pbtN_D33-QSHjBUpQaWcSc1xtL3k2aScGlSbOe-NVKq2L-RWUCQBRCLj1qkMYVx9DuDKMMyHPTR7uxAMBFd8EQX9Lj-wCAlAv_bQ'
									));
									//$data_json = json_encode($data);
									curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, 'POST');
									curl_setopt($ch2, CURLOPT_POSTFIELDS, $data_json);
									curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
									$response = curl_exec($ch2);
									curl_close($ch2);
									$resss = json_decode($response, true);
									if($resss['success'] == 1){
										$re .= '<br>設定されていたパソコンに通知が送信されました。';
									}
								}
								unset($value);
							} catch (Expection $e) {
								$result .= 'andSendCatch,';
								$message = 'データベースに接続できませんでした:' . $e;
							}
						}
					}
				} catch (Exception $e) {
					$reslt .= 'andCatch,';
					$message = 'データベースに接続できませんでした:' . $e;
				}
			}
		}
	}
	/*
	var_dump($reslt);
	var_dump($stmt);
	var_dump($date);
	var_dump($longitude);
	var_dump($latitude);
	var_dump($adress);
	var_dump($safety);
	var_dump($id);
	var_dump($res);
	var_dump($argres);
	var_dump($ad);
	var_dump($data);
	var_dump($data_json);
	var_dump($response);
	var_dump($_POST);
	var_dump($resss);
	*/
 ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130431814-1');
	</script>
	<meta charset="utf-8">
	<title>安否情報の送信 | 災害用伝言板</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
		fieldset.not-bottom-padding {
			padding-top: 30px;
			padding-bottom: 0px;
		}
		fieldset.bottom-padding {
			padding-bottom: 20px;
		}
	</style>
</head>
<body class="container">
	<h1>送信</h1>
	<!-- ユーザーIDにHTMLタグが含まれても良いようにエスケープする -->
	<?php if (isset($_SESSION['NAME'])): ?>
		<p class="gray-text center">ようこそ<u><?php echo htmlspecialchars($_SESSION["NAME"], ENT_QUOTES); ?></u>さん</p>	<!-- ユーザー名をechoで表示 -->
		<p class="center"><a href="https://system.ruther.tk/logout">ログアウト</a></p>
		<?php if(false/*!isset($_SESSION['LastUpdate'])*/): ?>
		<div class="row">
			<h4>初めての方へ</h4>
			<div class="col s2"></div>
			<div class="col s10">
				このサイトはドットゼミEXPO 2018で発表・リリースした通知付き災害用伝言板サービスです。
				<br>
				個人で運営しておりますので、完全なサービスを提供できない場合があります。詳しくは<a href="https://anpi.ruther.tk/tutorial" target="_blank">こちら</a>
			</div>
		</div>
		<?php endif;?>
		<div class="red-text center"><?php echo $message; ?></div>
		<div class="blue-text text-darken-3 center"><?php echo $re; ?></div>
		<br>
		<div class="col s12">
			<form method="POST" class="center row">
				<fieldset class="center not-bottom-padding">
					<legend>安否情報</legend>
					<select name="safety" required>
						<option selected disabled value="noselect">選択してください</option>
						<option value="genki">元気です</option>
						<option value="kega">けがをしています</option>
						<option value="help">救助が必要です</option>
					</select>
					<div class="input-field">
						<label for="memo">メモ</label>
						<textarea name="memo" id="memo" class="materialize-textarea" data-length="140"></textarea>
					</div>
					<div class="right">
						<a href="javascrit:void()" id="delete" title="メモをすべて消去する"><i class="material-icons">delete</i></a>
					</div>
				</fieldset>
				<br>
				<fieldset class="center col s12" style="padding-bottom: 20px; margin-bottom: 20px;">
					<legend>位置情報</legend>
					<button type="button" id="auto" class="btn waves-effect white black-text">
						位置情報を自動取得する
						<i class="material-icons left">location_searching</i>
					</button>
					<br>
					<label for="longitude">経度</label>
					<input type="text" name="longitude" id="longitude" value="<?php if(!empty($_POST['longitude'])) {echo $_POST['longitude'];} ?>" required>
					<label for="latitude">緯度</label>
					<input type="text" name="latitude" id="latitude" value="<?php if(!empty($_POST['latitude'])) {echo $_POST['latitude'];} ?>" required>
					<label for="adress">住所</label>
					<input type="text" name="adress" id="adress" value="<?php echo !empty($_POST['adress'])? $_POST['adress'] : 'このフィールドは自動的に入力されます' ?>">
				</fieldset>
				<button type="submit" value="送信する" class="btn waves-effect" name="submit" id="submit">
					送信する
					<i class="material-icons right">send</i>
				</button>
			</form>
		</div>
		<footer>
			<form action="../" class="center">
				<button type="submit" class="waves-effect white btn black-text">
					戻る
					<i class="material-icons left">arrow_back</i>
				</button>
			</form>
		</footer>
		<script>
			$(function(){
				$('select').formSelect();
				$('#AdressSet').on('click', function() {
					/*var aaa = $.ajax({
						type: 'GET',
						url: url,
						dataType: 'text',
						succcess: function(data){
							console.log(data);
							$('#adress').val(data);
						},
						error: function(data){
							console.warn('Error: ', data);
						}
					})
					console.log(aaa.responseText);
					*/
					//$('#adress').load(url);
					var lng = $('#longitude').val();
					var lat = $('#latitude').val();
					var url = 'https://anpi.ruther.tk/latlng?lat=' + lat + '&lng=' + lng + '&plane=1';
					console.log(url);
					$.get(url, function(result) {
						$('#adress').val(result);
					})
				})
				/*
				$('#LngLatSet').on('click', function(){
					var adress = $('#adress').val();
					var url = 'http://nomiphp.php.xdomain.jp/geocoding?q=' + adress;
					console.log(url);
					$.ajax({
						url: url,
						type: 'GET',
						dataType: 'json'
					})
					.then(
						function(result) {
							//var aaa = result.split(",");
							//var q = aaa[0];
							//var lng = aaa[1];
							//var lat = aaa[2];
							console.log(result);
							var rasf = $.parseJSON(result);
							console.debug(rasf);
							var q = result.q;
							var lng = result.lng;
							var lat = result.lat;
							console.log(lng);
							console.log(lat);
							$('#longitude').val(lng);
							$('#latitude').val(lat);
						},
						function(){
							console.warn(Errror);
						}
					);
				});
				*/
				$('#memo').characterCounter();
				$('#delete').on('click', () => {
					$('#memo').val('');
					$('#memo').removeClass('invalid');
					$('#memo').characterCounter();
				});
			});
		</script>
		<script>
			// Geolocation APIに対応している
			if (navigator.geolocation) {
				//alert("この端末では位置情報が取得できます");
			// Geolocation APIに対応していない
			} else {
				alert("この端末では位置情報が取得できません");
			}

			// 現在地取得処理
			$('#auto').on('click', function() {
				// 現在地を取得
				navigator.geolocation.getCurrentPosition(
					// 取得成功した場合
					function(position) {
						//alert("緯度:"+position.coords.latitude+",経度"+position.coords.longitude);
						$('#longitude').val(position.coords.longitude);
						$('#latitude').val(position.coords.latitude);
						var lng = $('#longitude').val();
						var lat = $('#latitude').val();
						var url = 'https://anpi.ruther.tk/latlng?lat=' + lat + '&lng=' + lng + '&plane=1';
						console.log(url);
						$.get(url, function(result) {
							$('#adress').val(result);
						
							$('#submit').addClass('pulse');
						})
					},
					// 取得失敗した場合
					function(error) {
						switch(error.code) {
						case 1: //PERMISSION_DENIED
							alert("位置情報の利用が許可されていません");
							break;
						case 2: //POSITION_UNAVAILABLE
							alert("現在位置が取得できませんでした");
							break;
						case 3: //TIMEOUT
							alert("タイムアウトになりました");
							break;
						default:
							alert("その他のエラー(エラーコード:"+error.code+")");
							break;
						}
					}
				);
				/*
					$('input[type=submit]').on('click', () => {
						modalContent();
						this.submit();
					});
				*/
			});
		</script>
		<script>
			$(function(){
				$('input[type=text] , select').on('input', function() {
					if($('#longitude').val() && $('#latitude').val() && $('#adress')) {
						$('#submit').addClass('pulse');
					}else {
						$('#submit').removeClass('pulse');
					}
				});
			});
			
		</script>
	<?php else: ?>
		<p class="center">ログイン、もしくは新規登録をしてください。</p>
		<p class="center"><a href="https://system.ruther.tk/signup">新規作成</a></p>
		<p class="center"><a href="https://system.ruther.tk/login">ログイン</a></p>
		<form action="../" class="center">
			<button type="submit" class="waves-effect white btn black-text">
				戻る
				<i class="material-icons left">arrow_back</i>
			</button>
		</form>
	<?php endif; ?>
</body>
</html>