const mapmaker = ($element, $lat, $lng) => {
	console.log($element, $lat, $lng);
	var control = new Y.SliderZoomControlVertical;
	var ymap = new Y.Map($element);
	ymap.drawMap(new Y.LatLng($lat, $lng), 17, Y.LayerSetId.NORMAL);

	
	var marker = new Y.Marker(new Y.LatLng($lat, $lng));
	var label = new Y.Label(new Y.LatLng($lat, $lng), "送信された位置");
	ymap.addFeature(marker);
	ymap.addFeature(label);

	ymap.addControl(control);
	ymap.setConfigure('scrollWhellZoom', true);

	$(window).resize(function(){
		var control = new Y.SliderZoomControlVertical;
		var ymap = new Y.Map($element);
		ymap.drawMap(new Y.LatLng($lat , $lng), 17, Y.LayerSetId.NORMAL);

		
		var marker = new Y.Marker(new Y.LatLng($lat, $lng));
		var label = new Y.Label(new Y.LatLng($lat, $lng), "送信された位置");
		ymap.addFeature(marker);
		ymap.addFeature(label);

		ymap.addControl(control);
		ymap.setConfigure('scrollWhellZoom', true);
	});
};