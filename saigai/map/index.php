<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	$_SESSION["url"] = 'https://anpi.ruther.tk/map/';
}else{
	$_SESSION["url"] = '';
}
?>
<?php 
	$rows;
	$row_count;
	$pagination;
	function h($string_html) {
		return htmlspecialchars($string_html, ENT_QUOTES);
	};
	function isSafety($safety) {
		switch ($safety) {
			case 'genki':
				# code...
				return '元気です';
				break;
			
			case 'kega':
				# code...
				return 'けがをしています';
				break;

			case 'help':
				# code...
				return '救助が必要です';
				break;

			default:
				# code...
				return false;
				break;
		}
	}

	function get_info($ua){
		$browser_name = $browser_version = $webkit_version = $platform = NULL;
		$is_webkit = false;
		//Browser
		if(preg_match('/Edge/i', $ua)){
			$browser_name = 'Edge';
			if(preg_match('/Edge\/([0-9.]* /', $ua, $match)){
				$browser_version = $match[1];
			}
		}elseif(preg_match('/(MSIE|Trident)/i', $ua)){
			$browser_name = 'IE';
			if(preg_match('/MSIE\s([0-9.]*)/', $ua, $match)){
				$browser_version = $match[1];
			}elseif(preg_match('/Trident\/7/', $ua, $match)){
				$browser_version = 11;
			}
		}elseif(preg_match('/Presto|OPR|OPiOS/i', $ua)){
			$browser_name = 'Opera';
			if(preg_match('/(Opera|OPR|OPiOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];		
		}elseif(preg_match('/Firefox/i', $ua)){
			$browser_name = 'Firefox';
			if(preg_match('/Firefox\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
		}elseif(preg_match('/Chrome|CriOS/i', $ua)){
			$browser_name = 'Chrome';
			if(preg_match('/(Chrome|CriOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];
		}elseif(preg_match('/Safari/i', $ua)){
			$browser_name = 'Safari';
			if(preg_match('/Version\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
		}
		//Webkit
		if(preg_match('/AppleWebkit/i', $ua)){
			$is_webkit = true;
			if(preg_match('/AppleWebKit\/([0-9.]*)/', $ua, $match)) $webkit_version = $match[1];
		}
		//Platform
		if(preg_match('/ipod/i', $ua)){
			$platform = 'iPod';
		}elseif(preg_match('/iphone/i', $ua)){
			$platform = 'iPhone';
		}elseif(preg_match('/ipad/i', $ua)){
			$platform = 'iPad';
		}elseif(preg_match('/android/i', $ua)){
			$platform = 'Android';
		}elseif(preg_match('/windows phone/i', $ua)){
			$platform = 'Windows Phone';
		}elseif(preg_match('/linux/i', $ua)){
			$platform = 'Linux';
		}elseif(preg_match('/macintosh|mac os/i', $ua)) {
			$platform = 'Mac';
		}elseif(preg_match('/windows/i', $ua)){
			$platform = 'Windows';
		}
		return array(
			'ua' => $ua,
			'browser_name' => $browser_name,
			'browser_version' => intval($browser_version),
			'is_webkit' => $is_webkit,
			'webkit_version' => intval($webkit_version),
			'platform' => $platform
		);
	}
	function isGarake($ua) {
		$return = '';
		if((strpos($ua, 'DoCoMo') !== false) || (strpos($ua, 'FOMA') !== false)) {
			$return = 'docomo';
			return true;
		}else if(strpos($ua, 'SoftBank') !== false) {
			$return = 'SoftBank';
			return true;
		}else {
			return false;
		}
	}
	function foreachkun($row, $page) {
		$i = 0;
		foreach($row as $now) {
			$i++;
			//var_dump($row);

			$lat = $now['latitude'];
			$lng = $now['longitude'];
			$ua = $now['ua'];
			$safety = $now['safety'];
			$add = $now['location'];
			$last = $now['updateDate'];
			$memo = $now['memo'];

			$browser_info = get_info($ua);
			$is = isGarake($_SERVER['HTTP_USER_AGENT']);
			$saf = isSafety($safety);

			$iss_saf = isset($saf)? $saf : '';
			$mb_add = isset($add)? mb_convert_encoding($add, 'UTF-8') : '';
			$latlng = (isset($lng) && isset($lat))? $lng . ' , ' . $lat : '';
			$lastup = isset($last)? $last : '';
			$brwsr_name = $browser_info['browser_name'];
			$platform = !empty($browser_info['browser_name'])? $browser_info['platform'] : '';
			$specialed_memo = h($memo);
			$i_X_page = $i + (($page - 1) * 10);

			echo <<<__HTML__
				<li class="s12">
					<div class="collapsible-header">
						{$last}
						<span class="badge">{$i_X_page}</span>
					</div>
					<div class="collapsible-body center">
						<div id="map{$i}" style="width: 100%; height: 300px;:" class="center"></div>
						<table class="highlight">
							<tr>
								<th>安否情報</th>
								<td>
									<table class="striped">
										<tr>
											<th class="blue-text text-darken-3">安否情報:</th>
											<td>{$iss_saf}</td>
										</tr>
										<tr>
											<th class="blue-text text-darken-3">住所:</th>
											<td>{$mb_add}</td>
										</tr>
										<tr>
											<th class="blue-text text-darken-3">経度・緯度:</th><td>{$latlng}</td>
										</tr>
										<tr>
											<th class="blue-text text-darken-3">最終更新:</th><td>{$lastup}</td>
										</tr>
										<tr>
											<th class="blue-text text-darken-3">メモ:<td><textarea class="materialize-textarea" style="height: 100px;">{$specialed_memo}</textarea></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<th>端末情報</th>
									<td>
									<table class="striped">
										<tr>
											<th class="blue-text text-darken-3">ブラウザ名:</th><td>{$brwsr_name}</td>
										</tr>
										<tr>
											<th class="blue-text text-darken-3">端末名:</th><td>{$platform}</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<a class="btn blue lighten-1 waves-effect waves-red grey-text text-lighten-5" target="_blank" href="//crisis.yahoo.co.jp/map/result?lat={$lat}&lon={$lng}&emergency=1">
							<i class="material-icons left">open_in_new</i>この安否情報が送信された付近の避難場所を検索する
						</a>
					</div>
				</li>
				<script>
					getmap('map{$i}', {$lat}, {$lng});
					latargs.push({$lat});
					lngargs.push({$lng});
				</script>
__HTML__;
		};
	};

	if(isset($_SESSION['ID'])) {
		$page = ((isset($_GET['page']) && ctype_digit($_GET['page']))? (int)$_GET['page'] : 1);
		$start = (($page > 1)? ($page * 10) - 10 : 0);
		//var_dump($page, $start);
		$err = '';
		$id = $_SESSION['ID'];
		$db['host'] = "localhost";		// DBサーバのURL
		$db['user'] = "nomiphp_ruther";		// ユーザー名
		$db['pass'] = "nomi051914";		// ユーザー名のパスワード
		$db['dbname'] = "nomiphp_login";		// データベース名

		try {
			$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			$stmt = $pdo->prepare('SELECT * FROM anpiData WHERE ownerID = :id LIMIT :start, 10');
			$stmt->bindParam(':start', $start, PDO::PARAM_INT);
			$stmt->bindParam(':id', $id, PDO::PARAM_INT);
			$stmt->execute();
			if($rows = $stmt->fetchAll(PDO::FETCH_ASSOC)){
				//var_dump($rows);
				$stmt_count = $pdo->prepare("SELECT COUNT(*) FROM anpiData WHERE ownerID = ?");
				$stmt_count->execute(array($id));
				$row_count = $stmt_count->fetchColumn();
				$pagination = ceil($row_count / 10);
				//var_dump($row_count, $pagination);
			}

		} catch (Exception $e) {
			$err = 'Error:' . $e;
		}
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130431814-1');
	</script>
	<meta charset="utf-8">
	<title>安否情報の確認 | 災害用伝言板</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Compiled and minified CSS -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<link rel=“stylesheet” href=“https://cdn.jsdelivr.net/npm/fontisto@v3.0.4/css/fontisto/fontisto.min.css”>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="icon" href="https://img.ruther.tk/icon.png">

	<script type="text/javascript" charset="utf-8" src="https://map.yahooapis.jp/js/V1/jsapi?appid=dj00aiZpPXVERERNdms1blEzNyZzPWNvbnN1bWVyc2VjcmV0Jng9NTU-"></script>
	<script type="text/javascript" charset="utf-8" src="https://anpi.ruther.tk/yolpfunction.js?<?php echo date("Y/m/d/H/i/s"); ?>"></script>
	<script type="text/javascript" charset="utf-8" src="https://anpi.ruther.tk/getmap.js?<?php echo date("Y/m/d/H/i/s"); ?>"></script>
</head>
<body class="container">
	<h1>安否情報の確認</h1>
	<?php if (isset($_SESSION['NAME'])): ?>
		<p class="gray-text center">ようこそ<u><?php echo htmlspecialchars($_SESSION["NAME"], ENT_QUOTES); ?></u>さん</p>		<!-- ユーザー名をechoで表示 -->
		<p class="center"><a href="https://system.ruther.tk/logout">ログアウト</a></p>
		<?php if(!empty($rows)): ?>
			<ul class="collapsible row">
				<script>
					const latargs = [];
					const lngargs = [];
				</script>
				<?php foreachkun($rows, $page); ?>
				<script>
					M.textareaAutoResize($('.materialize-textarea'));
				</script>
			</ul>
			<ul class="pagination center">
				<li class="<?php echo ($page === 1)? 'disabled': ''; ?>"><a href="<?php echo ($page === 1)? '#!' : (($page - 1 === 1)? '//anpi.ruther.tk/map/' : '?page=' . ($page - 1)); ?>"><i class="material-icons">chevron_left</i></a></li>
			<?php for($i = 0; $i < $pagination; $i++){ ?>
				<li class="<?php echo ($page === $i + 1)? 'active' : '';?>"><a href="<?php echo ($page === $i + 1)? '#!' : (($i + 1 === 1)? '//anpi.ruther.tk/map/' : '?page=' . ($i + 1)); ?>"><?php echo $i + 1; ?></a></li>
			<?php } ?>
				<li class="<?php echo ($page >= $pagination)? 'disabled' : '';?>"><a href="<?php echo ($page >= $pagination)? '#!' : '?page=' . ($page + 1); ?>"><i class="material-icons">chevron_right</i></a></li>
			</ul>
			<script>
				$(function (){
					var elem = document.querySelector('.collapsible');
					var instance = M.Collapsible.init(elem, {
						onOpenEnd: onOpenEndFunction(latargs, lngargs, latargs.length),
						outDuration: 150
					});
					$('.collapsible-header').on('click', () => {
						setTimeout(() => {
							onOpenEndFunction(latargs, lngargs, latargs.length);
						}, 100);
					});
				});
			</script>
		<?php endif; ?>
	<?php else: ?>
		<p class="center"><a href="https://system.ruther.tk/signup">新規作成</a></p>
		<p class="center"><a href="https://system.ruther.tk/login">ログイン</a></p>
	<?php endif; ?>
	<form action="//anpi.ruther.tk/" class="center" style="bottom: 0px">
		<button type="submit" class="btn waves-effect white black-text" value="元に戻る">
			戻る
			<i class="material-icons left">arrow_back</i>
		</button>
	</form>
</body>
</html>
