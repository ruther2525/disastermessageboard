<?php
/*
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	header("Location: ../Logout/");
	exit;
}
*/
?>
<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	$_SESSION["url"] = 'https://system.ruther.tk/main/';
}else{
	$_SESSION["url"] = '';
}
?>

<!doctype html>
<html>
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-130431814-1');
		</script>
		<meta charset="UTF-8">
		<title>メイン</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<!-- Compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
		<!-- Compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	</head>
	<body class="">
		<h1>メイン画面</h1>
		<!-- ユーザーIDにHTMLタグが含まれても良いようにエスケープする -->
		<?php if (isset($_SESSION['NAME'])): ?>
			<p class="gray-text center">ようこそ<u><?php echo htmlspecialchars($_SESSION["NAME"], ENT_QUOTES); ?></u>さん</p>  <!-- ユーザー名をechoで表示 -->
			<p class="center"><a href="../logout">ログアウト</a></p>
		<?php else: ?>
			<p class="center"><a href="../signup">新規作成</a></p>
			<p class="center"><a href="../login">ログイン</a></p>
		<?php endif; ?>
		<form action="https://anpi.ruther.tk" class="center">
			<input type="submit" class="btn-flat blue-text text-darken-2" value="災害安否情報を確認する">
		</form>
		<form action="https://ruther.tk/ppap/nokori" class="center">
			<input type="submit" value="残り課題を確認する" class="btn-flat blue-text text-darken-2">
		</form>
		<br>
		<hr>
		<form action="../mypage" class="center">
			<input type="submit" value="マイページ" class="btn-flat blue-text text-darken-2">
		</form>
		<form action="../option" class="center">
			<input type="submit" value="設定" class="btn-flat blue-text text-darken-2">
		</form>
	</body>
</html>