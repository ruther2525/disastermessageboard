<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	$isLogin = FALSE;
	$_SESSION['url'] = 'https://system.ruther.tk/option/';
	header('Location: https://system.ruther.tk/login');
}else{
	$isLogin = TRUE;
	$_SESSION['url'] = '';
}
?>
<?php 
function get_info($ua){
	$browser_name = $browser_version = $webkit_version = $platform = NULL;
	$is_webkit = false;
	//Browser
	if(preg_match('/Edge/i', $ua)){	
		$browser_name = 'Edge';
		if(preg_match('/Edge\/([0-9.]*/', $ua, $match)){
			$browser_version = $match[1]; 
		}
	}elseif(preg_match('/(MSIE|Trident)/i', $ua)){
		$browser_name = 'IE';
		if(preg_match('/MSIE\s([0-9.]*)/', $ua, $match)){
			$browser_version = $match[1];
		}elseif(preg_match('/Trident\/7/', $ua, $match)){
			$browser_version = 11;
		}
	}elseif(preg_match('/Presto|OPR|OPiOS/i', $ua)){
		$browser_name = 'Opera';
		if(preg_match('/(Opera|OPR|OPiOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];		
	}elseif(preg_match('/Firefox/i', $ua)){
		$browser_name = 'Firefox';
		if(preg_match('/Firefox\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
	}elseif(preg_match('/Chrome|CriOS/i', $ua)){
		$browser_name = 'Chrome';
		if(preg_match('/(Chrome|CriOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];
	}elseif(preg_match('/Safari/i', $ua)){
		$browser_name = 'Safari';
		if(preg_match('/Version\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
	}
	//Webkit
	if(preg_match('/AppleWebkit/i', $ua)){
		$is_webkit = true;
		if(preg_match('/AppleWebKit\/([0-9.]*)/', $ua, $match)) $webkit_version = $match[1];
	}
	//Platform
	if(preg_match('/ipod/i', $ua)){
		$platform = 'iPod';
	}elseif(preg_match('/iphone/i', $ua)){
		$platform = 'iPhone';
	}elseif(preg_match('/ipad/i', $ua)){
		$platform = 'iPad';
	}elseif(preg_match('/android/i', $ua)){
		$platform = 'Android';
	}elseif(preg_match('/windows phone/i', $ua)){
		$platform = 'Windows Phone';
	}elseif(preg_match('/linux/i', $ua)){	
		$platform = 'Linux';
	}elseif(preg_match('/macintosh|mac os/i', $ua)) {
		$platform = 'Mac';		
	}elseif(preg_match('/windows/i', $ua)){
		$platform = 'Windows';
	}
	switch ($platform) {
		case 'Android':
		case 'Linux':
		case 'Mac':
		case 'Windows':
			$is_notifi = TRUE;
			break;

		case 'iPod':
		case 'iPhone':
		case 'iPad':
		case 'Windows Phone':
			$is_notifi = FALSE;
			break;
			
		default:
			$is_notifi = FALSE;
			break;
	}
	return array(	
		'ua' => $ua,
		'browser_name' => $browser_name,
		'browser_version' => intval($browser_version),
		'is_webkit' => $is_webkit,
		'webkit_version' => intval($webkit_version),
		'platform' => $platform,
		'is_notifi' => $is_notifi
	);
}
function isGarake($ua) {
	$return = '';
	if((strpos($ua, 'DoCoMo') !== false) || (strpos($ua, 'FOMA') !== false)) {
		$return = 'docomo';
		return true;
	}else if(strpos($ua, 'SoftBank') !== false) {
		$return = 'SoftBank';
		return true;
	}else {
		return false;
	}
}
?>
<?php 
/*
class csrfValidator{
	const HASH_ALGO = 'sha256';

	public static function generate() {
		if(session_status() === PHP_SESSION_NONE) {
			throw new BadMethodCallException('Session is not active.');
		}
		return hash(self::HASH_ALGO, session_id());
	}
	public static function validate($token, $throw = FALSE) {
		$success = self::generate() === $token;
		if(!$success && $throw) {
			throw new RuntimeException('CSRF validation failed.', 400);
		}
		return $success;
	}
}*/
 ?>
<?php 
	header("Access-Control-Allow-Origin: *");
	//$token = $_POST['token'];

	function foreachkun($row) {
		$i = 0;
		if(empty($row)) {
			echo <<<__HTML__
				<li class="collection-item" id="noDevice">
					<div>デバイスが登録されていませんでした</div>
				</li>
				<script>
					//const deviceInt = {$i};
				</script>
__HTML__;
		}else{
			foreach($row as $now) {
				$i++;
				//var_dump($row);

				$tokenID = $now['id'];
				$ownerID = $now['ownerID'];
				$deviceName = $now['deviceName'];
				$token = $now['token'];
				$deviceToken = $now['deviceToken'];
				$ua = $now['ua'];

				$browser_info = get_info($ua);
				$is = isGarake($_SERVER['HTTP_USER_AGENT']);

				$brwsr_name = $browser_info['browser_name'];
				$platform = !empty($browser_info['browser_name'])? $browser_info['platform'] : '';

				echo <<<__HTML__
					<li class="collection-item">
						<div>{$deviceName}<a href="#!" class="secondary-content" id="{$i}"><i class="material-icons" title="この端末に通知を送る" id="icon{$i}">send</i></a></div>
					</li>
					<script>device_list.push({id: {$i}, deviceName: '{$deviceName}', deviceToken: '{$deviceToken}', token: '{$token}'});</script>
__HTML__;
			};
			echo <<<__HTML_SCRIPT__
					<script>
						//const deviceInt = {$i};
					</script>
__HTML_SCRIPT__;
		}
	};

	$id = $_SESSION['ID'];

	$message = '';

	$db['host'] = "localhost";  // DBサーバのURL
	$db['user'] = "nomiphp_ruther";  // ユーザー名
	$db['pass'] = "nomi051914";  // ユーザー名のパスワード
	$db['dbname'] = "nomiphp_login";  // データベース名

	$ua = $_SERVER['HTTP_USER_AGENT'];
	if (isset($_POST['submit'])) {
		try {
			//CsrfValidator::validate(filter_input(INPUT_POST, 'token'), true);
			try {
				//CsrfValidator::validate(filter_input(INPUT_POST, 'token'), true);
				$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
				//$stmt = $pdo->prepare("SELECT * FROM userData WHERE id = ?");
				//$stmt->execute(array($id));
				
				$notifi = (isset($_POST['notifi']))? $_POST['notifi'] : 'off';
				//var_dump($_POST);
				
				//$NToken = $_POST['notifiToken'] | null;
				
				$browser_info = get_info($ua);
				$is = isGarake($ua);

				//$ArrToken = array($NToken);
				//$SToken = serialize($ArrToken);

				$isN = function() {
					switch ($_POST['notifi']) {
						case 'on':
							return true;
							# code...
								break;
							
						case 'off':
							return false;
							# code...
							break;
					}
				};

				$stmt = $pdo->prepare("UPDATE userData SET isNotifi = ? WHERE id = ?");
				$stmt->execute(array(/*$SToken, */$ttpes = $notifi ==='on' ? true : false, $id));

				$re = "設定が完了しました";

				http_response_code(200);
			} catch (Exception $e) {
				http_response_code(503);
				$message = 'データベースに接続できませんでした:' . $e;
			}
		} catch (\RuntimeException $e) {
			header('Content-Type: text/plain; charset=UTF-8', true, $e->getCode() ?: 500);
			die($e->getMessage());
		}
	}

	if($_SERVER['REQUEST_METHOD'] != 'POST'){
		try{
			$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
			$stmt = $pdo->prepare('SELECT * FROM userData WHERE id = ? ;');
			$stmt->execute(array($id));

			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				// 入力したIDのユーザー名を取得
				$isNotifi = $row['isNotifi'];
				$notifi = $isNotifi;
				//var_dump($row);
			}
		} catch (Exception $e) {
			http_response_code(503);
			$message = 'データベースに接続できませんでした:' . $e;
		}
	}

	try {
		$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
		$devicestmt = $pdo->prepare('SELECT * FROM pushToken WHERE ownerID = ?');
		$devicestmt->execute(array($id));

		$rows = $devicestmt->fetchAll();
		//var_dump($rows);
	} catch (Exception $e) {
		$message = 'データベースに接続できませんでした:' . $e;
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130431814-1');
	</script>
	<meta charset="utf-8">
	<title>パソコンへの通知設定</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
		p.flash{
			animation: Flash 2s infinite;
		}

		/* アニメーション */
		@keyframes Flash{
			50%{
				text-shadow: 1px 1px 0 red;
			}
		}
		#preloader-overlay {
			content: '';
			display: none;
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0,0,0,0.6);
			z-index: 20000;
		}
		#preloader {
			display: none;
			position: fixed;
			z-index: 20001;
			overflow: show;
			margin: auto;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
		}
	</style>
	<link rel="manifest" href="/option/manifest.json">
</head>
<body>
	<h1>設定</h1>
	<!--<iframe src="https://pushnotification-c03b5.firebaseapp.com/" width="400" height="300" id="push-frame"></iframe>-->
	
	<?php if(isset($_SESSION['NAME'])): ?>
		<div class="blue-text text-darken-3 center"><?=!empty($re)? $re : '' ?></div>
		<div class="container">
			<div class="row">
				<div class="col s3 m12"></div>
				<div class="col s6 m12 card-panel red lighten-4 center" id="error" style="display: none; padding: 1em;"></div>
				<div class="col s3 m12"></div>
			</div>
			<div id="option" class="center row">
				<form method="POST" class="center">
					<div id="option" class="card darken-1">
						<div class="card-content">
							<div class="switch" style="padding-bottom: 30px;">
								<span class="card-title">安否情報が送信されたとき、PCに通知を送信する</span>
								<label>
									オフ<input type="checkbox" <?php switch ($notifi) {case 'on':case '1':echo 'checked';break;} ?> name="notifi" id="notifiswi"><span class="lever"></span>オン
								</label>
							</div>
							<ul class="collection with-header" id="registering_device">
								<li class="collection-header"><h4>登録中のデバイス</h4></li>
								<script>
									let device_list = [];
								</script>
								<?php foreachkun($rows); ?>
							</ul>
							<!-- Modal Trigger -->
							<a class="waves-effect waves-light btn modal-trigger" href="#modal1">追加する</a>
							<!-- Modal Structure -->
							<div id="modal1" class="modal bottom-sheet">
								<div class="modal-content"　id="one">
									<h4>通知デバイスにこのPCを追加</h4>
									<div class="input-field">
										<label for="RegisterDeviceName">デバイス名(必須)</label>
										<input type="text" class="" id="RegisterDeviceName">
										<span class="grey-text">このPCがわかるような名前をつけてください<br><b>Android端末, iOS端末は対応していません</b><br>Android端末の場合は通知用トークンの取得が成功しますが、一定期間で使用できなくなるため登録しないようにしてください</span>
									</div>
								</div>
								<div class="modal-footer">
									<a href="#!" class="modal-close waves-effect waves-green btn-flat" id="one_next">設定する</a>
								</div>
								<script>
									$(() => {
										const messaging = firebase.messaging();
										messaging.onMessage(payload => {
											console.log("Message received. ", payload);
											let notification = new Notification(payload.notification.title, {
												body: payload.notification.body,
												icon: payload.notification.icon
											});
											notification.addEventListener('click', () => {
												open('https://system.ruther.tk/option/');
												notification.close();
											});
											console.log(notification);
										});

										const getValue = localname => {
											return localStorage.getItem(localname);
										};

										const setValue = (localname, value) => {
											return localStorage.setItem(localname, value);
										};

										const deviceDataAttr = deviceToken => {
											if(typeof deviceToken === 'undefined') {
												return '';
											} 
											console.group('deviceDataAttr');
											console.log(deviceToken);
											let sameDevice = device_list.filter(list => {
												console.log(list, list.deviceToken === deviceToken);
												return list.deviceToken === deviceToken
											});
											let sameDeviceId = sameDevice[0].id;
											console.log(sameDevice, sameDeviceId);
											console.groupEnd('deviceDataAttr');
											$('#' + sameDeviceId).attr({disabled: 'disabled'});
											$('#' + sameDeviceId).prepend('<span class="grey-text">このPCです</span>');
											$('#icon' + sameDeviceId).html('face');
											$('#icon' + sameDeviceId).prop('title', '今お使いのPCです');
										}

										/**
										 * @param {string} token - FCMの通知用トークン
										 * @param {string} deviceName - webstorageに保存して自分の端末として認識させる用のトークン
										 */
										const sendNotification = (token, deviceName) => {
											$('#preloader, #preloader-overlay').fadeIn();
											console.log(token, deviceName);
											$.ajax({
												url: '/sendnotifi/',
												type: 'POST',
												data: {
													token: token,
													deviceName: deviceName
												}
											})
											.done(data => {
												console.log(data);
												console.log(data.token === token);
												if(data.response.failure === 1){
													$('#error').fadeIn();
													$('#error').html('<h5>通知の送信に失敗しました</h5>');
												}
												$('#preloader, #preloader-overlay').fadeOut();
											})
										}

										/**
										 * @param {string} token - FCMの通知用トークン
										 * @param {string} deviceName - 指定されたデバイス名
										 * @return {int, string, string, string} - (成功した場合)データベース上の管理ID, 送信したトークン, 送信したデバイス名, webstorageに保存して自分の端末として認識させる用のトークン
										 */
										const tokenSendAjax = (token, deviceName) => {
											console.log(token, deviceName);
											$.ajax({
												url: '/tokenpush/',
												type: 'POST',
												data: {
													token: token,
													deviceName: deviceName
												}
											})
											.done(data => {
												console.log(data);

												let json_decode = JSON.parse(JSON.stringify(data));

												console.log(json_decode);
												if(json_decode['response'] === 200){
													if(device_list.length <= 0) {
														$('#noDevice').remove();
													};

													if(json_decode['token'] === token){
														$('#registering_device').append('<li class="collection-item"><div>' + deviceName + '<a href="javascript:void()" class="secondary-content" id="' + (device_list.length + 1) + '"><i class="material-icons" title="この端末に通知を送る" id="icon' + (device_list.length + 1) + '">send</i></a></div></li>');
														device_list.push({id: device_list.length + 1, deviceName: json_decode['deviceName'], deviceToken: json_decode['deviceToken'], token: json_decode['token']});
														setValue('deviceToken', json_decode['deviceToken']);

														return json_decode;
													}
												}
											})
											.fail(data => {
												alert('エラーが発生しました。インターネットに接続しているか確認してください。:');
												console.error(data);
											})
										}

										//プッシュ通知パーミッション取得
										var requestPermission = instance => {
											messaging.requestPermission()
											.then(function() {
												//ユーザー毎のトークンを取得して画面に表示する
												messaging.getToken()
												.then(token => {
													const registerdevicename = $('#RegisterDeviceName').val();
													console.log('Token refreshed.');
													console.log("token "+ token);
													tokenSendAjax(token, registerdevicename);
													
													//instance.close();
													$('#preloader, #preloader-overlay').fadeOut();
												})
												.catch(err => {
													console.log('Unable to retrieve refreshed token ', err);
												});
											})
											.catch(err => {
												console.log('Unable to get permission to notify.', err);
												alert('通知を許可してください。');
											});
										}
										var instance = M.Modal.getInstance(document.querySelectorAll('.modal'));
										$('#one_next').on('click', () => {
											$('#preloader, #preloader-overlay').fadeIn();
											requestPermission(instance);
										})

										if(device_list.length > 0){
											deviceDataAttr(getValue('deviceToken'));
											for (let i = 0; i < device_list.length; i++) {
												const element = device_list[i];
												console.log(element);
												$('#' + element['id']).on('click', () => {
													if(element['token'] !== '' && element['deviceName'] !== ''){
														sendNotification(element['token'], element['deviceName']);
													}
												});
											}
										}
										
									})
								</script>
								<div id="preloader-overlay" class="valign-wrapper">
									<div id="preloader" class="preloader-wrapper big active">
										<div class="spinner-layer spinner-blue">
											<div class="circle-clipper left">
											<div class="circle"></div>
											</div><div class="gap-patch">
											<div class="circle"></div>
											</div><div class="circle-clipper right">
											<div class="circle"></div>
											</div>
										</div>

										<div class="spinner-layer spinner-red">
											<div class="circle-clipper left">
											<div class="circle"></div>
											</div><div class="gap-patch">
											<div class="circle"></div>
											</div><div class="circle-clipper right">
											<div class="circle"></div>
											</div>
										</div>

										<div class="spinner-layer spinner-yellow">
											<div class="circle-clipper left">
											<div class="circle"></div>
											</div><div class="gap-patch">
											<div class="circle"></div>
											</div><div class="circle-clipper right">
											<div class="circle"></div>
											</div>
										</div>

										<div class="spinner-layer spinner-green">
											<div class="circle-clipper left">
											<div class="circle"></div>
											</div><div class="gap-patch">
											<div class="circle"></div>
											</div><div class="circle-clipper right">
											<div class="circle"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<?php //var_dump($browser_info); echo '<br>'; var_dump($row); echo '<br>'; var_dump($id); ?>
					<p class="flash" id="NotSave">変更があります 保存をしてください</p>
					<button class="btn waves-effect waves-light red" type="submit" name="submit" value="submit">保存する<!--<i class="material-icons right">send</i>--></button>
				</form>
			</div>
		</div>
	</form>
	<br>
	<form action="../main" class="center">
		<button type="submit" class="btn waves-effect white black-text">
			戻る
			<i class="material-icons left">arrow_back</i>
		</button>
	</form>
	<!-- The core Firebase JS SDK is always required and must be listed first -->
	<script src="https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js"></script>
	<script>
		// Your web app's Firebase configuration
		var firebaseConfig = {
			apiKey: "AIzaSyCkfPpitVtt2pksZHcE5bKpgAiO9ZZoepA",
			projectId: "pushnotification-c03b5",
			messagingSenderId: "330249774748",
			appId: "1:330249774748:web:067f5f040a94d04e"
		};
		// Initialize Firebase
		firebase.initializeApp(firebaseConfig);
	</script>
	<script>
		var openW;
		$(function(){
			$('#NotSave').hide();
			$('#notifi').on('change', function(){
				$('#NotSave').animate({display: 'toggle'});
				console.log(document.getElementById('notifiswi').checked);
			});
		})
	</script>
	<!--
		<script>
			$(function() {
				/*
				var ifrm = document.getElementById('push-frame').contentWindow;
				ifrm.postMessage('Hello!', 'https://pushnotification-c03b5.firebaseapp.com/index.html');
				*/
				var win = window.open('https://pushnotification-c03b5.firebaseapp.com/index.html', 'notification', 'width=400, height=300')
			})
		</script>
	-->
	<script>
		$('.modal').modal();
	</script>
	<?php else: ?>
		<p class="center"><a href="../signup">新規作成</a></p>
		<p class="center"><a href="../login">ログイン</a></p>
	<?php endif; ?>
</body>
</html>