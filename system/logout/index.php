<?php
session_start();
if (isset($_SESSION["NAME"])) {
    $errorMessage = "ログアウトしました。";
} else {
    $errorMessage = "セッションがタイムアウトしました。";
}

// セッションの変数のクリア
$_SESSION = array();

// セッションクリア
@session_destroy();
session_start();
if(isset($_SERVER['HTTP_REFERER'])){
	$url = $_SERVER['HTTP_REFERER'];
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ログアウト</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    </head>
    <body>
        <h1>ログアウト画面</h1>
        <div><?php echo htmlspecialchars($errorMessage, ENT_QUOTES); ?></div>
        <ul>
            <li><a href="../login/">ログイン画面に戻る</a></li>
            <?php if(!empty($url)) : ?>
            	<li><a href="<?php echo $url; ?>">元の画面(<?php echo $url; ?>)に戻る</a></li>
            <?php endif; ?>
        </ul>
    </body>
</html>