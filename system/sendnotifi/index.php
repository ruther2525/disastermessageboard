<?php

//POSTデータ
$token = (isset($_POST['token']))? str_replace('"', '', $_POST['token']) : '';
$deviceName = (isset($_POST['deviceName']))? $_POST['deviceName'] : '';
$body = (isset($_POST['bodyText']))? $_POST['bodyText'] : 'この通知はアカウントの設定画面の送信ボタンによって送信されました。';
$click_action = (isset($_POST['click_action']))? $_POST['click_action'] : 'https://system.ruther.tk/testoption.php';

if($token !== '' && $deviceName !== ''){
	$url = 'https://fcm.googleapis.com/fcm/send';
	$ch = curl_init();
	$data = array(
		'notification' => array(
			'title' => 'https://system.ruther.tk/',
			'body' => $body . '     あなたのデバイス名は' . (String)$deviceName . 'です。',
			'click_action' => $click_action
		),
		'to' => $token
	);
	$data_json = json_encode($data);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'content-type: application/json',
		'authorization: key=AAAATORrppw:APA91bHdcfJtDbTVsy-JSdgiSkQH-TF6cyBmXaX94FAdTxvL0U1QCDVgFyzCy0pbtN_D33-QSHjBUpQaWcSc1xtL3k2aScGlSbOe-NVKq2L-RWUCQBRCLj1qkMYVx9DuDKMMyHPTR7uxAMBFd8EQX9Lj-wCAlAv_bQ'
	));
	//$data_json = json_encode($data);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$response = curl_exec($ch);
	curl_close($ch);
	$resss = json_decode($response, true);
	header("Content-Type: application/json; charset=utf-8");
	echo json_encode(array(
		'token' => $token,
		'response' => $resss
	));
} else{
	header("Content-Type: application/json; charset=utf-8");
	echo json_encode(
		array(
			'response' => 400,
			'error' => 'No Paramator.',
			'paramator' => array('token' => $token, 'device' => $deviceName)
		)
	);
}