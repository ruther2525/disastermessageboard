<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	$isLogin = FALSE;
	$_SESSION['url'] = 'https://system.ruther.tk/option';
	header('Location: https://system.ruther.tk/login');
}else{
	$isLogin = TRUE;
	$_SESSION['url'] = '';
}
?>
<?php 
function get_info($ua){
	$browser_name = $browser_version = $webkit_version = $platform = NULL;
	$is_webkit = false;
	//Browser
	if(preg_match('/Edge/i', $ua)){	
		$browser_name = 'Edge';
		if(preg_match('/Edge\/([0-9.]*/', $ua, $match)){
			$browser_version = $match[1]; 
		}
	}elseif(preg_match('/(MSIE|Trident)/i', $ua)){
		$browser_name = 'IE';
		if(preg_match('/MSIE\s([0-9.]*)/', $ua, $match)){
			$browser_version = $match[1];
		}elseif(preg_match('/Trident\/7/', $ua, $match)){
			$browser_version = 11;
		}
	}elseif(preg_match('/Presto|OPR|OPiOS/i', $ua)){
		$browser_name = 'Opera';
		if(preg_match('/(Opera|OPR|OPiOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];		
	}elseif(preg_match('/Firefox/i', $ua)){
		$browser_name = 'Firefox';
		if(preg_match('/Firefox\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
	}elseif(preg_match('/Chrome|CriOS/i', $ua)){
		$browser_name = 'Chrome';
		if(preg_match('/(Chrome|CriOS)\/([0-9.]*)/', $ua, $match)) $browser_version = $match[2];
	}elseif(preg_match('/Safari/i', $ua)){
		$browser_name = 'Safari';
		if(preg_match('/Version\/([0-9.]*)/', $ua, $match)) $browser_version = $match[1];
	}
	//Webkit
	if(preg_match('/AppleWebkit/i', $ua)){
		$is_webkit = true;
		if(preg_match('/AppleWebKit\/([0-9.]*)/', $ua, $match)) $webkit_version = $match[1];
	}
	//Platform
	if(preg_match('/ipod/i', $ua)){
		$platform = 'iPod';
	}elseif(preg_match('/iphone/i', $ua)){
		$platform = 'iPhone';
	}elseif(preg_match('/ipad/i', $ua)){
		$platform = 'iPad';
	}elseif(preg_match('/android/i', $ua)){
		$platform = 'Android';
	}elseif(preg_match('/windows phone/i', $ua)){
		$platform = 'Windows Phone';
	}elseif(preg_match('/linux/i', $ua)){	
		$platform = 'Linux';
	}elseif(preg_match('/macintosh|mac os/i', $ua)) {
		$platform = 'Mac';		
	}elseif(preg_match('/windows/i', $ua)){
		$platform = 'Windows';
	}
	switch ($platform) {
		case 'Android':
		case 'Linux':
		case 'Mac':
		case 'Windows':
			$is_notifi = TRUE;
			break;

		case 'iPod':
		case 'iPhone':
		case 'iPad':
		case 'Windows Phone':
			$is_notifi = FALSE;
			break;
			
		default:
			$is_notifi = FALSE;
			break;
	}
	return array(	
		'ua' => $ua,
		'browser_name' => $browser_name,
		'browser_version' => intval($browser_version),
		'is_webkit' => $is_webkit,
		'webkit_version' => intval($webkit_version),
		'platform' => $platform,
		'is_notifi' => $is_notifi
	);
}
function isGarake($ua) {
	$return = '';
	if((strpos($ua, 'DoCoMo') !== false) || (strpos($ua, 'FOMA') !== false)) {
		$return = 'docomo';
		return true;
	}else if(strpos($ua, 'SoftBank') !== false) {
		$return = 'SoftBank';
		return true;
	}else {
		return false;
	}
}
?>
<?php 
class csrfValidator{
	const HASH_ALGO = 'sha256';

	public static function generate() {
		if(session_status() === PHP_SESSION_NONE) {
			throw new BadMethodCallException('Session is not active.');
		}
		return hash(self::HASH_ALGO, session_id());
	}
	public static function validate($token, $throw = FALSE) {
		$success = self::generate() === $token;
		if(!$success && $throw) {
			throw new RuntimeException('CSRF validation failed.', 400);
		}
		return $success;
	}
}
 ?>
<?php 
	header("Access-Control-Allow-Origin: *");
	//$token = $_POST['token'];

	$id = $_SESSION['ID'];

	$message = '';

	$db['host'] = "localhost";  // DBサーバのURL
	$db['user'] = "nomiphp_ruther";  // ユーザー名
	$db['pass'] = "nomi051914";  // ユーザー名のパスワード
	$db['dbname'] = "nomiphp_login";  // データベース名

	$ua = $_SERVER['HTTP_USER_AGENT'];
	if (isset($_POST['submit'])) {
		try {
			//CsrfValidator::validate(filter_input(INPUT_POST, 'token'), true);
			try {
				//CsrfValidator::validate(filter_input(INPUT_POST, 'token'), true);
				$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
				//$stmt = $pdo->prepare("SELECT * FROM userData WHERE id = ?");
				//$stmt->execute(array($id));
				
				$notifi = $_POST['notifi'];
				//var_dump($_POST);
				
				$NToken = $_POST['notifiToken'];
				
				$browser_info = get_info($ua);
				$is = isGarake($ua);

				$ArrToken = array($NToken);
				$SToken = serialize($ArrToken);

				$isN = function() {
					switch ($_POST['notifi']) {
						case 'on':
							return true;
							# code...
								break;
							
						case 'off':
							return false;
							# code...
							break;
					}
				};

				$stmt = $pdo->prepare("UPDATE userData SET  notifiToken = ? , isNotifi = ? WHERE id = ?");
				$stmt->execute(array($SToken, $ttpes = $notifi ==='on' ? true : false, $id));

				$re = "設定が完了しました";

				http_response_code(200);
			} catch (Exception $e) {
				http_response_code(503);
				$message = 'データベースに接続できませんでした:' . $e;
			}
		} catch (\RuntimeException $e) {
			header('Content-Type: text/plain; charset=UTF-8', true, $e->getCode() ?: 500);
			die($e->getMessage());
		}
	}else{
		//http_response_code(400);
	}
	/*$isChe = function() {
		
	};*/

	if($_SERVER['REQUEST_METHOD'] != 'POST' ){
		try{
			$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
			$stmt = $pdo->prepare('SELECT * FROM userData WHERE id = ? ;');
			$stmt->execute(array($id));

			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				// 入力したIDのユーザー名を取得
				$isNotifi = $row['isNotifi']; 
				$notifi = $isNotifi;
				$NToken = unserialize($row['notifiToken']);
				//var_dump($row);
			}
		} catch (Exception $e) {
			http_response_code(503);
			$message = 'データベースに接続できませんでした:' . $e;
		}
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130431814-1');
	</script>
	<meta charset="utf-8">
	<title>パソコンへの通知設定</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
		p.flash{
			animation: Flash 2s infinite;
		}

		/* アニメーション */
		@keyframes Flash{
			50%{
				text-shadow: 1px 1px 0 red;
			}
		}
	</style>
</head>
<body>
	<h1>設定</h1>
	<!--<iframe src="https://pushnotification-c03b5.firebaseapp.com/" width="400" height="300" id="push-frame"></iframe>-->
	
	<?php if(isset($_SESSION['NAME'])): ?>
		<div class="blue-text text-darken-3 center"><?=!empty($re)? $re : '' ?></div>
		<div class="container">
			<div id="option" class="center row">
				<form method="POST" class="center">
					<div id="option">
						<div id="notifi">
							<div class="switch">
								<label class="col s6">アカウントに安否情報が送信されたとき、PCに通知を送信する</label>
								<label class="col s6">
									オフ<input type="checkbox" <?php switch ($notifi) {case 'on':case '1':echo 'checked';break;} ?> name="notifi" id="notifiswi"><span class="lever"></span>オン
								</label>
							</div>

						</div>
					</div>
					<!--<input type="hidden" name="token" value="<?=CsrfValidator::generate()?>">-->
					<!--<input type="submit" value="保存する" class="btn-flat waves-effect">-->
					<br>
					<?php //var_dump($browser_info); echo '<br>'; var_dump($row); echo '<br>'; var_dump($id); ?>
					<input type="hidden" id="notifiToken" name="notifiToken" value="<?=$NToken[0]?>">
					<p class="flash" id="NotSave">変更があります 保存をしてください</p>
					<button class="btn waves-effect waves-light red" type="submit" name="submit" value="submit">保存する<!--<i class="material-icons right">send</i>--></button>
				</form>
			</div>
		</div>
	</form>
	<br>
	<form action="../main" class="center">
		<button type="submit" class="btn waves-effect white black-text">
			戻る
			<i class="material-icons left">arrow_back</i>
		</button>
	</form>
	<script>
		var openW;
		$(function(){
			$('#NotSave').hide();
			$('#notifi').on('change', function(){
				$('#NotSave').show();
				console.log(document.getElementById('notifiswi').checked);
				if(document.getElementById('notifiswi').checked === true){
					
					//window.open('https://pushnotification-c03b5.firebaseapp.com/index.html', '_blank', 'width=180px, height="320, mwnubar=no, toolbar=no, location=no, status=no, scrollbars=no, resizable=no');
					openW = window.open('https://pushnotification-c03b5.firebaseapp.com/index.html', '_blank', 'width=180px, height="320');
				} else {
					$('#NotSave').hide();
					$('#notifiToken').val('');
					if(openW !== undefined){
						openW.postMessage('CLOSE', 'https://pushnotification-c03b5.firebaseapp.com/index.html');
					}
				}
			})
			function receiveMessage(event) {
				console.log(event);
				// このメッセージの送信者は信頼している者か？
				if (event.origin !== "https://pushnotification-c03b5.firebaseapp.com") {
					console.warn('ERROR: Not Match Origin');
					return;
				}
				// event.source は window.opener
				// event.data は "hello there!"
				// 受け取ったメッセージの生成元を確かめたい場合（どんな場合でもそうするべ
				// きです）、メッセージに返答するための便利なイディオムは event.source 上
				// の postMessage を呼び出し、targetOrigin に event.origin を指定すること
				// です。
				if(event.data === "SUCCESS"){
					event.source.postMessage(
						"GET TXTIITOKEN",
						event.origin
					);
				}
				if(event.data.match(/^TOKEN:/)){
					var token = event.data.slice(6);
					console.log(token);
					$('#notifiToken').val(token);
					event.source.postMessage(
						"OK TOKEN",
						event.origin
					);
				}
			}
			window.addEventListener("message", receiveMessage, false);
		})
	</script>
	<!--
		<script>
			$(function() {
				/*
				var ifrm = document.getElementById('push-frame').contentWindow;
				ifrm.postMessage('Hello!', 'https://pushnotification-c03b5.firebaseapp.com/index.html');
				*/
				var win = window.open('https://pushnotification-c03b5.firebaseapp.com/index.html', 'notification', 'width=400, height=300')
			})
		</script>
	-->
	<?php else: ?>
		<p class="center"><a href="../signup">新規作成</a></p>
		<p class="center"><a href="../login">ログイン</a></p>
	<?php endif; ?>
</body>
</html>