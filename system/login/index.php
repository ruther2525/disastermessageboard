<?php
//require 'password.php';   // password_verfy()はphp 5.5.0以降の関数のため、バージョンが古くて使えない場合に使用
// セッション開始
session_start();

$db['host'] = "localhost";  // DBサーバのURL
$db['user'] = "nomiphp_ruther";  // ユーザー名
$db['pass'] = "nomi051914";  // ユーザー名のパスワード
$db['dbname'] = "nomiphp_login";  // データベース名
// エラーメッセージの初期化
$errorMessage = "";

isset($_SESSION['url'])? $url = $_SESSION['url'] : '';

// ログインボタンが押された場合
if (isset($_POST["login"])) {
	// 1. ユーザIDの入力チェック
	if (empty($_POST["username"])) {  // emptyは値が空のとき
		$errorMessage = 'ユーザー名が未入力です。';
	} else if (empty($_POST["password"])) {
		$errorMessage = 'パスワードが未入力です。';
	}

	if (!empty($_POST["username"]) && !empty($_POST["password"])) {
		// 入力したユーザIDを格納
		$username = $_POST["username"];

		// 2. ユーザIDとパスワードが入力されていたら認証する
		$dsn = sprintf('mysql: host=%s; dbname=%s; charset=utf8', $db['host'], $db['dbname']);
		// 3. エラー処理
		try {
			//$pdo = new PDO($dsn, $db['user'], $db['pass'], array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
			$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
			$stmt = $pdo->prepare('SELECT * FROM userData WHERE name = ?');
			$stmt->execute(array($username));

			$password = $_POST["password"];

			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				if (password_verify($password, $row['password'])) {
					session_regenerate_id(true);

					// 入力したIDのユーザー名を取得
					$id = $row['id'];
					$sql = "SELECT * FROM userData WHERE id = $id";  //入力したIDからユーザー名を取得
					$stmt = $pdo->prepare($sql);
					foreach ($stmt as $row) {
						$row['name'];  // ユーザー名
					}
					$_SESSION["NAME"] = $row['name'];
					$_SESSION["ISADMIN"] = $row['isAdmin'];
					$_SESSION["ID"] = $id;
					$_SESSION['LastUpdate'] = $row['LastUpdate'];
					$_SESSION['isAdmin'] = $row['isAdmin'];

					$date = new DateTime();
					$date = $date->format('Y-m-d H:i:s');

					$stmt = $pdo->prepare("UPDATE userData SET LastLogin = ? WHERE id = ?");
					$stmt->execute(array($date, $id));

					if(empty($url)){
						header("Location: ../main/");  // メイン画面へ遷移
					}else{
						header("Location: " . $url);
					}
					exit();  // 処理終了
				} else {
					// 認証失敗
					$errorMessage = 'ユーザーIDあるいはパスワードに誤りがあります。';
				}
			} else {
				// 4. 認証成功なら、セッションIDを新規に発行する
				// 該当データなし
				$errorMessage = 'ユーザーIDあるいはパスワードに誤りがあります。';
			}
		} catch (PDOException $e) {
			$errorMessage = 'データベースエラー';
			//$errorMessage = $sql;
			// $e->getMessage() でエラー内容を参照可能（デバッグ時のみ表示）
			// echo $e->getMessage();
		}
	}
}else if(isset($_SESSION["NAME"])){
	header("Location: https://system.ruther.tk/main");
}
?>

<!doctype html>
<html>
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-130431814-1');
		</script>
		<meta charset="UTF-8">
		<title>ログイン</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		 <!-- Compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
		<!-- Compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	</head>
	<body class="container">
		<h1>ログイン画面</h1>
		<form id="loginForm" name="loginForm" action="" method="POST" class="center row">
			<fieldset class="col s12" style="padding-bottom: 10px;">
				<legend>ログインフォーム</legend>
				<div><font color="#ff0000"><?php echo htmlspecialchars($errorMessage, ENT_QUOTES); ?></font></div>
				<label for="userid">ユーザー名</label><input type="text" id="username" name="username" placeholder="ユーザーネームを入力" value="<?php if (!empty($_POST["username"])) {echo htmlspecialchars($_POST["username"], ENT_QUOTES);} ?>">
				<br>
				<label for="password">パスワード</label><input type="password" id="password" name="password" value="" placeholder="パスワードを入力">
				<br>
				<input type="submit" id="login" name="login" value="ログイン" class="btn-flat">
				<div class="grey-text center" style="border: solid 3px #aaaa; border-radius: 5px; padding: 5px">
					ユーザー名とパスワードを入力して、[ログイン] ボタンを押してください
				</div>
			</fieldset>
		</form>
		<br>
		<form action="../signup/" class="center row">
			<fieldset class="s12">
				<legend>新規登録フォーム</legend>
				<input type="submit" value="新規登録" class="btn-flat">
			</fieldset>
		</form>
	</body>
</html>