<?php
session_start();

// ログイン状態チェック
if (!isset($_SESSION["NAME"])) {
	//changed from testothersend.php
	http_response_code(401);
	header('Location: https://system.ruther.tk/login');
}else{
	$_SESSION["url"] = '';
}
?>
<?php
$db['host'] = "localhost";  // DBサーバのURL
$db['user'] = "nomiphp_ruther";  // ユーザー名
$db['pass'] = "nomi051914";  // ユーザ名のパスワード
$db['dbname'] = "nomiphp_login";  // データベース名

//とりまUA
$ua = $_SERVER['HTTP_USER_AGENT'];

//セッション
$user = (isset($_SESSION['NAME']))? $_SESSION['NAME'] : '';
$ownerID = $_SESSION['ID'];


//POSTデータ
$token = (isset($_POST['token']))? $_POST['token'] : '';
$deviceName = (isset($_POST['deviceName']))? $_POST['deviceName'] : '';
//削除フラグは後になりそう
$isDelete = (isset($_POST['isdelete']))? $_POST['isdelete'] : 'no';
$id = (isset($_POST['tokenID']))? $_POST['tokenID'] : '';

if($user !== '' && $token !== '' && $deviceName !== ''){
	try {
		$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		if($isDelete === 'yes'){
			//tokenを削除
			$stmt = $pdo->prepare('UPDATE pushToken SET isDelete = yes WHERE id = ?, ownerID = ?');
			$isExecuteSuccess = $stmt->execute(array($id, $ownerID));
			$response = $isExecuteSuccess? 200 : 500;
		}else{
			$date = new Datetime();

			$deviceToken = password_hash($user . 'nnaaaaaaaaaaaa' . $date->format('Y-m-d H:i:s'), PASSWORD_DEFAULT);
			
			$stmt = $pdo->prepare('INSERT pushToken SET ownerID = ?, token = ?, deviceToken = ?, deviceName = ?, ua = ?');
			$isExecuteSuccess = $stmt->execute(array($ownerID, $token, $deviceToken, $deviceName, $ua));
			$response = $isExecuteSuccess? 200 : 500;
		}
	}catch(Expection $e) {
		$errMsg = 'エラー: ' . $e;
	}

	header("Content-Type: application/json; charset=utf-8");
	$json_out = (!isset($errMsg))? json_encode(
		array(
			'response' => $response,
			'token' => $token,
			'deviceToken' => $deviceToken,
			'tokenID' => $token
		)
	) : json_encode(
		array(
			'response' => $response,
			'error' => $errMsg
		)
	);
	echo $json_out;
}else{
	header("Content-Type: application/json; charset=utf-8");
	echo json_encode(
		array(
			'response' => 400,
			'error' => 'No Paramator.',
			'paramator' => array('token' => $token, 'device' => $deviceName)
		)
	);
}