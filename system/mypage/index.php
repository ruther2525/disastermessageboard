<?php
	//開発時はすべてのエラーを表示する
	//error_reporting(E_ALL | E_STRICT);
	session_start();

	// ログイン状態チェック
	if (!isset($_SESSION["NAME"])) {
		$_SESSION["url"] = 'https://system.ruther.tk/mypage';
		header("Location: https://system.ruther.tk/login");
	}else{
		$_SESSION["url"] = '';
	}
?>
<?php 
	$db['host'] = "localhost";  // DBサーバのURL
	$db['user'] = "nomiphp_ruther";  // ユーザー名
	$db['pass'] = "nomi051914";  // ユーザー名のパスワード
	$db['dbname'] = "nomiphp_login";  // データベース名

	$id = $_SESSION['ID'];

	try {
		//$pdo = new PDO($dsn, $db['user'], $db['pass'], array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
		$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
		$stmt = $pdo->prepare('SELECT * FROM userData WHERE id = ?');
		$stmt->execute(array($id));

		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			// 入力したIDのユーザー名を取得
			$sql = "SELECT * FROM userData WHERE id = $id";  //入力したIDからユーザー名を取得
			$stmt = $pdo->prepare($sql);
			foreach ($stmt as $row) {
				$row['name'];  // ユーザー名
			}
			

			$lat = $row['latitude'];
			$lng = $row['longitude'];
			$ua = $row['ua'];
			$safety = $row['safety'];
			$add = $row['location'];
			$last = $row['LastUpdate'];
			$ll = $row['LastLogin'];
			$isnotifi = $row['isNotifi'];
				//exit();  // 処理終了
		} else {
			// 4. 認証成功なら、セッションIDを新規に発行する
			// 該当データなし
			$errorMessage = 'ユーザーIDあるいはパスワードに誤りがあります。';
		}
	} catch (PDOException $e) {
		$errorMessage = 'データベースエラー';
		//$errorMessage = $sql;
		// $e->getMessage() でエラー内容を参照可能（デバッグ時のみ表示）
		// echo $e->getMessage();
	}

	function isSafety($safety) {
		switch ($safety) {
			case 'genki':
				# code...
				return '元気です';
				break;
			
			case 'kega':
				# code...
				return 'けがをしています';
				break;
			case 'help':
				# code...
				return '救助が必要です';
				break;
			default:
				# code...
				return false;
				break;
		}
	};
	function IsNotifi($value) {
		switch ($value) {
			case true:
				return 'オン';
				break;
			case false:
				return 'オフ';
				break;
		}
	};
	$ISN = IsNotifi($isnotifi);
	$safe = isSafety($safety);
 ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130431814-1');
	</script>
	<meta charset="utf-8">
	<title>マイページ</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
	<h1>マイページ</h1>
	<?php if (isset($_SESSION['NAME'])): ?>
		<p class="gray-text center">ようこそ<u><?=htmlspecialchars($_SESSION["NAME"], ENT_QUOTES);?></u>さん</p>	<!-- ユーザー名をechoで表示 -->
		<p class="center"><a href="../logout">ログアウト</a></p>
		<div class="container center">
			<div class="row">
				<div class="col s12">
					<div class="card grey darken-1">
						<div class="card-content white-text">
							<span class="card-title">アカウント情報</span>
							<p><table>
								<tr>
									<th>名前</th>
									<td>
										<?=htmlspecialchars($_SESSION["NAME"], ENT_QUOTES);?>
									</td>
								</tr>
								<tr>
									<th>最終ログイン</th>
									<td>
										<?=htmlspecialchars($ll, ENT_QUOTES);?>
									</td>
								</tr>
							</table></p>
						</div>
						<div class="card-action">
							<a href="../option">設定する</a>
						</div>
					</div>
					<div class="card grey darken-1">
						<div class="card-content white-text">
							<span class="card-title">災害用伝言版</span>
							<p><table>
								<tr>
									<th>安否情報</th>
									<td>
										<?=htmlspecialchars($safe, ENT_QUOTES);?>
									</td>
								</tr>
								<tr>
									<th>最終更新</th>
									<td>
										<?=htmlspecialchars($last, ENT_QUOTES);?>
									</td>
								</tr>
								<tr>
									<th>通知設定</th>
									<td>
										<?=htmlspecialchars($ISN, ENT_QUOTES);?>
									</td>
								</tr>
							</table></p>
						</div>
						<div class="card-action">
							<a href="https://anpi.ruther.tk/sebd">安否情報を送信する</a>
							<a href="https://anpi.ruther.tk/map">安否情報を確認する</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<form action="../main" class="center">
			<button type="submit" class="btn white black-text waves-effect" value="元に戻る">
				戻る
				<i class="material-icons left">arrow_back</i>
			</button>
	</form>
</body>
</html>