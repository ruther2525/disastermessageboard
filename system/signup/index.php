<?php
session_start();

//開発時はすべてのエラーを表示する
error_reporting(E_ALL | E_STRICT);

$db['host'] = "localhost";  // DBサーバのURL
$db['user'] = "nomiphp_ruther";  // ユーザー名
$db['pass'] = "nomi051914";  // ユーザー名のパスワード
$db['dbname'] = "nomiphp_login";  // データベース名
// エラーメッセージ、登録完了メッセージの初期化
$errorMessage = "";
$signUpMessage = "";

if (isset($_POST["signUp"])) {
	if (!empty($_POST["username"]) && !empty($_POST["password"]) && !empty($_POST["password2"]) && $_POST["password"] === $_POST["password2"]) {
		// 入力したユーザIDとパスワードを格納
		$username = $_POST["username"];
		$password = $_POST["password"];
		//echo 1;

		try {
			$pdo = new PDO('mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'] , $db['user'] , $db['pass'] );
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			
			$stmt = $pdo->prepare('SELECT COUNT(*) FROM userData WHERE `name` = ?');
			$stmt->execute(array($username));
			//echo 2;
			if($count = $stmt->fetch(PDO::FETCH_ASSOC)) {
				//echo 3;
				//var_dump($count);
				if($count["COUNT(*)"] === '0') {
					//echo 4;
					try {
						//echo 5;
						$insert = $pdo->prepare("INSERT userData SET name = ?, password = ?, location = ?, longitude = ?, latitude = ?, safety = ?, ua = ?, notifiToken = ?, notifiDetail = ?");
						$insert->execute(array($username, password_hash($password, PASSWORD_DEFAULT), '', '', '', '', $_SERVER['HTTP_USER_AGENT'], '', ''));
						$userid = $pdo->lastinsertid();  // 登録した(DB側でauto_incrementした)IDを$useridに入れる
						
						$signUpMessage = '登録が完了しました。下にある戻るボタンから戻り、ログインしてください。';
					} catch (PDOExpection $e) {
						$errorMessage .= 'データベースエラー<br>';
						$devMessage = $e->getMessage();
					}
				}else{
					$errorMessage .= '<span class="bold">既に登録されているユーザー名です。</span><br>';
				}
			}
		} catch (PDOException $e) {
			$errorMessage = 'データベースエラー';
			$devMessage = $e->getMessage();
		}
	} else if($_POST["password"] != $_POST["password2"]) {
		$errorMessage .= 'パスワードに誤りがあります。<br>';
	}
	if (!isset($_POST["username"]) || empty($_POST["username"])) {  // 値が空のとき
		$errorMessage .= 'ユーザーIDが未入力です。<br>';
	}
	if (!isset($_POST["password"]) || empty($_POST["password"])) {
		$errorMessage .= 'パスワードが未入力です。<br>';
	}
	if (!isset($_POST["password2"]) || empty($_POST["password2"])) {
		$errorMessage .= 'パスワードを再入力してください。<br>';
	}
	if (!($_POST["password"] === $_POST["password2"])) {
		$errorMessage .= 'パスワードが異なります。もう一度入力してください。<br>';
	}
}
?>
<!doctype html>
<html>
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130431814-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-130431814-1');
		</script>
		<meta charset="UTF-8">
		<title>新規登録</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
		<!-- Compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	<body class="container">
		<h1>新規登録</h1>
		<form id="loginForm" name="loginForm" action="" method="POST" class="row">
			<fieldset class="col s8 m12" style="padding-bottom: 10px;">
				<legend>新規登録フォーム</legend>
				<?php if(isset($errorMessage) || !empty($errorMessage)):?><div><span style="color: #ff0000"><?=$errorMessage?></span></div><?php endif;?>
				<?php if(isset($devMessage) || !empty($devMessage)):?><div><span style=color:#ff0000"><?=$devMessage?></span><?php endif;?>
				<?php if(isset($signUpMessage) || !empty($signUpMessage)):?><div><span style="color: #0000ff"><?=$signUpMessage?></span></div><?php endif;?>
				<label for="username">ユーザー名</label><input type="text" id="username" name="username" placeholder="ユーザー名を入力" value="<?php if (!empty($_POST["username"])) {echo htmlspecialchars($_POST["username"], ENT_QUOTES);} ?>">
				<br>
				<label for="password">パスワード</label><input type="password" id="password" name="password" value="" placeholder="パスワードを入力">
				<br>
				<label for="password2">パスワード(確認用)</label><input type="password" id="password2" name="password2" value="" placeholder="再度パスワードを入力" onkeyup="javascript:if(($('#passsword').val() !== '') && ($('#password2').val() !== '') && $('#password').val() !== $('#password2').val()){$('#passerror').html('パスワードが一致しません').fadeIn();$(this).addClass('invalid');}else{$('#passerror').fadeOut().html('');$(this).removeClass('invalid');}"onkeydown="javascript:if(($('#passsword').val() !== '') && ($('#password2').val() !== '') && $('#password').val() !== $('#password2').val()){$('#passerror').html('パスワードが一致しません').fadeIn();$(this).addClass('invalid');}else{$('#passerror').fadeOut().html('');$(this).removeClass('invalid');}">
				<span class="red-text" id="passerror"></span>
				<br>
				<button type="submit" id="signUp" name="signUp" class="waves-effect btn">
					新規登録
					<i class="material-icons right">send</i>						
				</button>
				<br>
				<br>
				<div class="grey-text center" style="border: solid 3px #aaaa; border-radius: 5px; padding: 5px">
					ユーザー名とパスワードを入力して、[新規登録<i class="material-icons tiny">send</i>]ボタンを押してください
				</div>
			</fieldset>
		</form>
		<div class="col s4 m12"></div>
		<br>
		<form action="../login/">
			<button type="submit" class="waves-effect white btn black-text">
				戻る
				<i class="material-icons left">arrow_back</i>
			</button>
		</form>
	</body>
</html>